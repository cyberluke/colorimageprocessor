import api.ColorRecognitionProcessor;
import api.ImageResult;
import lombok.extern.slf4j.Slf4j;
import net.openhft.affinity.Affinity;
import net.openhft.affinity.AffinityLock;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.testng.annotations.Test;
import processor.HistogramProcessor;
import processor.MedianCutProcessor;
import processor.SimpleProcessor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

@Slf4j
public class MedianCutTestSingleThread {

    @Test(dataProvider = "data-provider", dataProviderClass = TestSuiteProvider.class)
    public void theTest(String[] urls) throws IOException {

        String fileName = "output.csv";
        FileUtils.deleteQuietly(new File(fileName));
        final ColorRecognitionProcessor sp = new MedianCutProcessor();
        //final ColorRecognitionProcessor sp = new HistogramProcessor();
        sp.setIgnoreWhite(true);
        sp.setMaxColors(3);
        sp.setQuality(1); // quality is defined as number of increments in for loop per pixel (1 = 100%, 10 = read every tenth pixel)

        //ExecutorService executor = Executors.newSingleThreadExecutor();

        try (AffinityLock al = AffinityLock.acquireLock()) {
            try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
                for (String imageUrl : urls) {
                    URL url = new URL(imageUrl);
                    try {
                        BufferedImage img = ImageIO.read(url);

                        final int[] result = sp.process(img);

                        //executor.submit(new Runnable() {
                        //    public void run() {
                        try {
                            bufferedWriter.write(imageUrl);
                            for (int color : result) {
                                bufferedWriter.write(",");
                                int r = (color >> 16) & 0xFF;
                                int g = (color >> 8) & 0xFF;
                                int b = color & 0xFF;
                                bufferedWriter.write(String.format("#%02x%02x%02x", r, g, b));
                            }
                            bufferedWriter.newLine();
                            bufferedWriter.flush();
                        } catch (IOException e) {
                            log.error(e.getMessage());
                        }
                        //   }
                        //});
                    } catch (IOException e) {
                        log.error("Cannot read file: {}", imageUrl);
                    }

                }

            }
        }

    }
}
