import api.ColorRecognitionProcessor;
import lombok.extern.slf4j.Slf4j;
import net.openhft.affinity.AffinityLock;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;
import processor.MedianCutProcessor;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

@Slf4j
public class MedianCutTestParallel {

    @Test(dataProvider = "data-provider", dataProviderClass = TestSuiteProvider.class)
    public void theTest(String[] urls) throws IOException {

        String fileName = "output.csv";
        FileUtils.deleteQuietly(new File(fileName));
        final BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false));

        Arrays.stream(urls).parallel().forEach(imageUrl -> {
            try {
                final ColorRecognitionProcessor sp = new MedianCutProcessor();
                //final ColorRecognitionProcessor sp = new HistogramProcessor();
                sp.setIgnoreWhite(true);
                sp.setMaxColors(3);
                sp.setQuality(1); // quality is defined as number of increments in for loop per pixel (1 = 100%, 10 = read every tenth pixel)

                BufferedImage img = null;
                img = ImageIO.read(new URL(imageUrl));

                final int[] result = sp.process(img);

                try {
                    String outputStr = imageUrl;
                    for (int color : result) {
                        outputStr += ",";
                        int r = (color >> 16) & 0xFF;
                        int g = (color >> 8) & 0xFF;
                        int b = color & 0xFF;
                        outputStr += String.format("#%02x%02x%02x", r, g, b);
                    }
                    bufferedWriter.write(outputStr + "\n");
                    bufferedWriter.flush();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            } catch (IOException e) {
                log.error("File not found: {}", imageUrl);
            }
        });

    }
}
