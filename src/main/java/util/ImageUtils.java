package util;

import lombok.extern.slf4j.Slf4j;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.Arrays;

@Slf4j
public class ImageUtils {

    public static int[] getPixels(BufferedImage image, int quality, boolean ignoreWhite) {

        switch (image.getType()) {
            case BufferedImage.TYPE_3BYTE_BGR:
                return asBGR(image, quality, ignoreWhite);
            case BufferedImage.TYPE_4BYTE_ABGR:
                return asABGR(image, quality, ignoreWhite);
            default:
                log.info("Image type not optimized for speed: {}", image.getType());
                return asRGB(image, quality, ignoreWhite);
        }
    }

    protected static int[] asABGR(BufferedImage image, int quality, boolean ignoreWhite) {
        int a;
        int r, g, b, offset;
        int pixelsCount = image.getWidth() * image.getHeight();
        byte[] pixelDataIn = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        int[] pixelDataOut = new int[(pixelsCount + quality - 1) / quality];
        int realPixelCount = 0;

        for (int i = 0; i < pixelsCount; i += quality) {
            offset = i * 4;
            a = pixelDataIn[offset] & 0xFF;
            b = pixelDataIn[offset + 1] & 0xFF;
            g = pixelDataIn[offset + 2] & 0xFF;
            r = pixelDataIn[offset + 3] & 0xFF;

            if (a > 127 && ! (ignoreWhite && isWhite(r, g, b))) {
                pixelDataOut[realPixelCount] = (r << 16) | (g << 8) | (b);
                realPixelCount++;
            }
        }
        return Arrays.copyOfRange(pixelDataOut, 0, realPixelCount);
    }

    protected static int[] asBGR(BufferedImage image, int quality, boolean ignoreWhite) {
        int r, g, b, offset;
        int pixelsCount = image.getWidth() * image.getHeight();
        byte[] pixelDataIn = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        int[] pixelDataOut = new int[(pixelsCount + quality - 1) / quality];
        int realPixelCount = 0;

        for (int i = 0; i < pixelsCount; i += quality) {
            offset = i * 3;
            b = pixelDataIn[offset] & 0xFF;
            g = pixelDataIn[offset + 1] & 0xFF;
            r = pixelDataIn[offset + 2] & 0xFF;

            if (! (ignoreWhite && isWhite(r, g, b))) {
                pixelDataOut[realPixelCount] = (r << 16) | (g << 8) | (b);
                realPixelCount++;
            }
        }

        return Arrays.copyOfRange(pixelDataOut, 0, realPixelCount);
    }

    protected static int[] asRGB(BufferedImage image, int quality, boolean ignoreWhite) {
        int r, g, b;
        int pixelsCount = image.getWidth() * image.getHeight();
        int[] pixelDataOut = new int[(pixelsCount + quality - 1) / quality];
        int realPixelCount = 0;
        int w = image.getWidth();
        int h = image.getHeight();

        for (int i = 0; i < pixelsCount; i += quality) {
            int pixel = image.getRGB(i % w, i / h);

            r = (pixel >> 16) & 0xff;
            g = (pixel >> 8) & 0xff;
            b = (pixel & 0xff);

            if (! (ignoreWhite && isWhite(r, g, b))) {
                pixelDataOut[realPixelCount] = pixel;
                realPixelCount++;
            }

        }

        return Arrays.copyOfRange(pixelDataOut, 0, realPixelCount);
    }

    public static boolean isWhite(int r, int g, int b) {
        return r > 250 && g > 250 && b > 250;
    }
}
