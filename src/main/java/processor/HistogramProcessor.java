package processor;

import api.ColorRecognitionProcessor;
import api.ImageResult;
import api.InstanceManager;
import api.ParsedColor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.org.apache.commons.lang3.ArrayUtils;
import util.ImageUtils;

import java.awt.image.BufferedImage;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Data
public class HistogramProcessor implements ColorRecognitionProcessor {

    protected int maxColors = 3;
    protected int quality = 1;
    protected boolean ignoreWhite = true;
    protected int threshold = 50; // for HSV rounding
    protected static int maxCount = 0;

    public int[] process(BufferedImage image) {
        int r, g, b;
        int[] pixels = ImageUtils.getPixels(image, quality, ignoreWhite);
        Map<Integer, HSVColor> histogram = new HashMap<>(threshold);
        float[] hsv = new float[3];

        for (int pixel : pixels) {
            r = (pixel >> 16) & 0xff;
            g = (pixel >> 8) & 0xff;
            b = (pixel & 0xff);

            if (ignoreWhite && ImageUtils.isWhite(r, g, b)) {
                continue;
            }

            ParsedColor.RGBToHSV(r, g, b, hsv);

            // Calculate colorfulness and filter by these
            if (hsv[1] * hsv[2] > 0.5f) {
                int key = (int) (hsv[0] * threshold);
                HSVColor hsvColor = histogram.getOrDefault(key, null);
                if (null == hsvColor) {
                    hsvColor = new HSVColor(hsv[0], hsv[1], hsv[2]);
                    histogram.put(key, hsvColor);
                } else {
                    hsvColor.increment();
                    if (maxCount < hsvColor.getCount()) {
                        maxCount = hsvColor.getCount();
                    }
                }
            }
        }

        //log.info("Max count: {}", maxCount);

        Iterator<HSVColor> iterator = histogram.values().stream().sorted(Comparator.comparing(HSVColor::getCount, Comparator.reverseOrder())).limit(maxColors).iterator();

        int cnt = 0;
        int[] finalResult = new int[maxColors];
        while (iterator.hasNext()) {
            HSVColor entry = iterator.next();
            //log.info("count {}, colorfulness: {} - {}", entry.count, entry.getColorfulness(), InstanceManager.newImageResult(ParsedColor.HSVtoRGB(entry.h,entry.s,entry.v)).getDominantColor().toHex());
            finalResult[cnt] = ParsedColor.HSVtoRGB(entry.h,entry.s,entry.v);
            cnt++;
        }

        return finalResult;
    }

    private static class HSVColor {
        private int count = 0;
        private float h, s, v;

        HSVColor(float h, float s, float v) {
            this.h = h;
            this.s = s;
            this.v = v;
        }

        public void increment() {
            count++;
        }

        public int getCount() {
            return count;
        }

        public float getH() {
            return h;
        }

        public float getS() {
            return s;
        }

        public float getV() {
            return v;
        }

        public float getColorfulness() {
            return getS() * getV();
        }

        public float getWeight() {
            return weightedAverage(getColorfulness(), 2f, getCount() / (float) maxCount, 1f);
        }

        static float weightedAverage(float... values) {
            assert values.length % 2 == 0;

            float sum = 0;
            float sumWeight = 0;

            for (int i = 0; i < values.length; i += 2) {
                float value = values[i];
                float weight = values[i + 1];

                sum += (value * weight);
                sumWeight += weight;
            }

            return sum / sumWeight;
        }
    }
}
