package processor;

import api.ColorRecognitionProcessor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import util.ImageUtils;

import java.awt.image.BufferedImage;
import java.util.*;

@Slf4j
@Data
public class SimpleProcessor implements ColorRecognitionProcessor {

    protected int maxColors = 3;
    protected int quality = 1;
    protected boolean ignoreWhite = false;
    protected int threshold = 3; // for average color dithering

    public int[] process(BufferedImage image) {

        Map<Integer, Integer> rgbData = new HashMap<>();

        int r, g, b;
        int[] pixels = ImageUtils.getPixels(image, quality, ignoreWhite);
        for (int pixel : pixels) {
            r = (pixel >> 16) & 0xff;
            g = (pixel >> 8) & 0xff;
            b = (pixel & 0xff);

            r = (int) (Math.round(threshold * r / 255.0) * (255.0 / threshold));
            g = (int) (Math.round(threshold * g / 255.0) * (255.0 / threshold));
            b = (int) (Math.round(threshold * b / 255.0) * (255.0 / threshold));

            if (ignoreWhite && ImageUtils.isWhite(r, g, b)) {
                continue;
            }
            //int pixel8bit = (r * 7 / 255) << 5 + (g * 7 / 255) << 2 + (b * 3 / 255);

            pixel = (r << 16) | (g << 8) | (b);

            if (rgbData.containsKey(pixel)) {
                rgbData.put(pixel, rgbData.get(pixel) + 1);
            } else {
                rgbData.put(pixel, 1);
            }
        }
        Iterator<Map.Entry<Integer, Integer>> iterator = (rgbData.entrySet().stream().sorted((c1, c2) -> c2.getValue().compareTo(c1.getValue())).limit(maxColors).iterator());

        int cnt = 0;
        int[] finalResult = new int[maxColors];
        while (iterator.hasNext()) {
            Map.Entry<Integer, Integer> entry = iterator.next();
            finalResult[cnt] = entry.getKey();
            cnt++;
        }

        return finalResult;
    }
}
