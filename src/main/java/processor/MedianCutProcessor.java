package processor;

import api.ColorRecognitionProcessor;
import api.ImageResult;
import api.InstanceManager;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import process.MedianCutQuantizer;
import util.ImageUtils;

import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

@Slf4j
@Data
public class MedianCutProcessor implements ColorRecognitionProcessor {

    private int maxColors = 3;
    private int quality = 1;
    private boolean ignoreWhite = true;

    @Override
    public int[] process(BufferedImage image) {
        final int[] rgbPixels = ImageUtils.getPixels(image, quality, ignoreWhite);

        final MedianCutQuantizer mcq = new MedianCutQuantizer(rgbPixels, maxColors);

        MedianCutQuantizer.ColorNode[] result = mcq.getQuantizedColors();

        Arrays.sort(result, Comparator.comparingInt(MedianCutQuantizer.ColorNode::getCount).reversed());

        //log.info("Number of quantized colors: {}", mcq.countQuantizedColors());

        int[] finalResult = new int[maxColors];
        int cnt = 0;
        for (MedianCutQuantizer.ColorNode color : result) {
            //log.info("count: {}, color: {}", color.getCount(), InstanceManager.newImageResult(color.getRgb()).getDominantColor().toHex());
            finalResult[cnt] = color.getRgb();
            cnt++;
        }
        return finalResult;
    }
}
