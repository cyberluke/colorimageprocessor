package api;

import java.awt.image.BufferedImage;

public interface ColorRecognitionProcessor {
    void setIgnoreWhite(boolean ignoreWhite);
    void setMaxColors(int max);
    void setQuality(int quality);
    int[] process(BufferedImage image);
}
