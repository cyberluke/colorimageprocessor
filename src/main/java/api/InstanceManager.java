package api;

public class InstanceManager {

    // @TODO implement flyweight design pattern
    public static ImageResult newImageResult(int color) {
        return new ImageResultImpl(color);
    }

    public static ParsedColor newParsedColor(int color) {
        return new ParsedColorImpl(color);
    }
}
