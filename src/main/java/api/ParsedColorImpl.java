package api;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class ParsedColorImpl implements ParsedColor {

    int R;
    int G;
    int B;

    public ParsedColorImpl(final int color) {
        this.R = (color >> 16) & 0xFF;
        this.G = (color >> 8) & 0xFF;
        this.B = color & 0xFF;
    }

    public int distanceTo(final int color) {
        final int deltaR = this.R - ((color & 0xff000000) >>> 24);
        final int deltaG = this.G - ((color & 0x00ff0000) >>> 16);
        final int deltaB = this.B - ((color & 0x0000ff00) >>> 8);
        return (deltaR * deltaR) + (deltaG * deltaG) + (deltaB * deltaB);
    }

    public int[] getRGB() {
        log.warn("getRGB() not efficient as reading individual RGB");
        return new int[]{this.R, this.G, this.B};
    }

    public int getPixel() {
        return (this.R << 16) | (this.G << 8) | (this.B);
    }

    public double[] toCIELab() {

        double r, g, b, X, Y, Z, xr, yr, zr;

        // D65/2°
        double Xr = 95.047;
        double Yr = 100.0;
        double Zr = 108.883;


        // --------- RGB to XYZ ---------//

        r = R/255.0;
        g = G/255.0;
        b = B/255.0;

        if (r > 0.04045)
            r = Math.pow((r+0.055)/1.055,2.4);
        else
            r = r/12.92;

        if (g > 0.04045)
            g = Math.pow((g+0.055)/1.055,2.4);
        else
            g = g/12.92;

        if (b > 0.04045)
            b = Math.pow((b+0.055)/1.055,2.4);
        else
            b = b/12.92 ;

        r*=100;
        g*=100;
        b*=100;

        X =  0.4124*r + 0.3576*g + 0.1805*b;
        Y =  0.2126*r + 0.7152*g + 0.0722*b;
        Z =  0.0193*r + 0.1192*g + 0.9505*b;


        // --------- XYZ to Lab --------- //

        xr = X/Xr;
        yr = Y/Yr;
        zr = Z/Zr;

        if ( xr > 0.008856 )
            xr =  (float) Math.pow(xr, 1/3.);
        else
            xr = (float) ((7.787 * xr) + 16 / 116.0);

        if ( yr > 0.008856 )
            yr =  (float) Math.pow(yr, 1/3.);
        else
            yr = (float) ((7.787 * yr) + 16 / 116.0);

        if ( zr > 0.008856 )
            zr =  (float) Math.pow(zr, 1/3.);
        else
            zr = (float) ((7.787 * zr) + 16 / 116.0);


        double[] lab = new double[3];

        lab[0] = (116*yr)-16;
        lab[1] = 500*(xr-yr);
        lab[2] = 200*(yr-zr);

        return lab;

    }

    public String toHex() {
        return String.format("#%02x%02x%02x", R, G, B);
    }
}
