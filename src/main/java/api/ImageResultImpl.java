package api;

public class ImageResultImpl implements ImageResult {
    ParsedColor parsedColor;

    public ImageResultImpl(int color) {
        this.parsedColor = InstanceManager.newParsedColor(color);
    }

    public ParsedColor getDominantColor() {
        return this.parsedColor;
    }
}
