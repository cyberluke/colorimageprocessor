package api;

public interface ImageResult {
    ParsedColor getDominantColor();
}
