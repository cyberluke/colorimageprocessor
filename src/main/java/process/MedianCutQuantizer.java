package process;

import api.ParsedColor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * This sample code is made available as part of the book "Digital Image
 * Processing - An Algorithmic Introduction using Java" by Wilhelm Burger
 * and Mark J. Burge, Copyright (C) 2005-2008 Springer-Verlag Berlin,
 * Heidelberg, New York.
 * Note that this code comes with absolutely no warranty of any kind.
 * See http://www.imagingbook.com for details and licensing conditions.
 * <p>
 * Adapted and modified by Lukas Satin
 * <p>
 * Full Description:
 * This is an implementation of Heckbert's median-cut color quantization algorithm
 * (Heckbert P., "Color Image Quantization for Frame Buffer Display", ACM Transactions
 * on Computer Graphics (SIGGRAPH), pp. 297-307, 1982).
 * Unlike in the original algorithm, no initial uniform (scalar) quantization is used to
 * for reducing the number of image colors. Instead, all colors contained in the original
 * image are considered in the quantization process. After the set of representative
 * colors has been found, each image color is mapped to the closest representative
 * in RGB color space using the Euclidean distance.
 * The quantization process has two steps: first a ColorQuantizer object is created from
 * a given image using one of the constructor methods provided. Then this ColorQuantizer
 * can be used to quantize the original image or any other image using the same set of
 * representative colors (color table).
 */

@Slf4j
public class MedianCutQuantizer {

    private ColorNode[] imageColors = null;    // original (unique) image colors
    private ColorNode[] quantColors = null;    // quantized colors

    public MedianCutQuantizer(int[] pixels, int KMax) {
        quantColors = findRepresentativeColors(pixels, KMax);
    }

    public int countQuantizedColors() {
        return quantColors.length;
    }

    public ColorNode[] getQuantizedColors() {
        return quantColors;
    }

    ColorNode[] findRepresentativeColors(int[] pixels, int Kmax) {
        ColorHistogram colorHist = new ColorHistogram(pixels);
        int K = colorHist.getNumberOfColors();
        ColorNode[] rCols = null;

        imageColors = new ColorNode[K];
        for (int i = 0; i < K; i++) {
            int rgb = colorHist.getColor(i);
            int cnt = colorHist.getCount(i);
            imageColors[i] = new ColorNode(rgb, cnt);
        }

        if (K <= Kmax) {
            // image has fewer colors than Kmax
            rCols = imageColors;
        } else {
            ColorBox initialBox = new ColorBox(0, K - 1, 0);
            List<ColorBox> colorSet = new ArrayList<ColorBox>();
            colorSet.add(initialBox);
            int k = 1;
            boolean done = false;
            while (k < Kmax && ! done) {
                ColorBox nextBox = findBoxToSplit(colorSet);
                if (nextBox != null) {
                    ColorBox newBox = nextBox.splitBox();
                    colorSet.add(newBox);
                    k = k + 1;
                } else {
                    done = true;
                }
            }
            rCols = averageColors(colorSet);
        }
        return rCols;
    }

    public void quantizeImage(@NotNull int[] pixels) {
        for (int i = 0; i < pixels.length; i++) {
            ColorNode color = findClosestColor(pixels[i]);
            pixels[i] = ParsedColor.rgb(color.red, color.green, color.blue);
        }
    }

    ColorNode findClosestColor(int rgb) {
        int idx = findClosestColorIndex(rgb);
        return quantColors[idx];
    }

    int findClosestColorIndex(int rgb) {
        int red = ParsedColor.red(rgb);
        int green = ParsedColor.green(rgb);
        int blue = ParsedColor.blue(rgb);
        int minIdx = 0;
        int minDistance = Integer.MAX_VALUE;
        for (int i = 0; i < quantColors.length; i++) {
            ColorNode color = quantColors[i];
            int d2 = color.distance2(red, green, blue);
            if (d2 < minDistance) {
                minDistance = d2;
                minIdx = i;
            }
        }
        return minIdx;
    }

    private ColorBox findBoxToSplit(@NotNull List<ColorBox> colorBoxes) {
        ColorBox boxToSplit = null;
        // from the set of splitable color boxes
        // select the one with the minimum level
        int minLevel = Integer.MAX_VALUE;
        for (ColorBox box : colorBoxes) {
            if (box.colorCount() >= 2) {    // box can be split
                if (box.level < minLevel) {
                    boxToSplit = box;
                    minLevel = box.level;
                }
            }
        }
        return boxToSplit;
    }

    @NotNull
    private ColorNode[] averageColors(@NotNull List<ColorBox> colorBoxes) {
        int n = colorBoxes.size();
        ColorNode[] avgColors = new ColorNode[n];
        int i = 0;
        for (ColorBox box : colorBoxes) {
            avgColors[i] = box.getAverageColor();
            i = i + 1;
        }
        return avgColors;
    }

    // -------------- class ColorNode -------------------------------------------

    public static class ColorNode {

        private final int red, green, blue;
        private final int count;

        private float[] hsv;

        ColorNode(int rgb, int count) {
            this.red = ParsedColor.red(rgb);
            this.green = ParsedColor.green(rgb);
            this.blue = ParsedColor.blue(rgb);
            this.count = count;
        }

        ColorNode(int red, int green, int blue, int count) {
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.count = count;
        }

        public int getRgb() {
            return ParsedColor.rgb(red, green, blue);
        }

        public float[] getHsv() {
            if (hsv == null) {
                hsv = new float[3];
                ParsedColor.RGBToHSV(red, green, blue, hsv);
            }
            return hsv;
        }

        public int getCount() {
            return count;
        }

        int distance2(int red, int grn, int blu) {
            // returns the squared distance between (red, grn, blu)
            // and this this color
            int dr = this.red - red;
            int dg = this.green - grn;
            int db = this.blue - blu;
            return dr * dr + dg * dg + db * db;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName() +
                    " #" + Integer.toHexString(getRgb()) +
                    ". count: " + count;
        }
    }

    // -------------- class ColorBox -------------------------------------------

    class ColorBox {

        int lower = 0;     // lower index into 'imageColors'
        int upper = - 1;    // upper index into 'imageColors'
        int level;         // split level o this color box
        int count = 0;     // number of pixels represented by thos color box
        int rmin, rmax;    // range of contained colors in red dimension
        int gmin, gmax;    // range of contained colors in green dimension
        int bmin, bmax;    // range of contained colors in blue dimension

        ColorBox(int lower, int upper, int level) {
            this.lower = lower;
            this.upper = upper;
            this.level = level;
            this.trim();
        }

        int colorCount() {
            return upper - lower;
        }

        void trim() {
            // recompute the boundaries of this color box
            rmin = 255;
            rmax = 0;
            gmin = 255;
            gmax = 0;
            bmin = 255;
            bmax = 0;
            count = 0;
            for (int i = lower; i <= upper; i++) {
                ColorNode color = imageColors[i];
                count = count + color.count;
                int r = color.red;
                int g = color.green;
                int b = color.blue;
                if (r > rmax) {
                    rmax = r;
                }
                if (r < rmin) {
                    rmin = r;
                }
                if (g > gmax) {
                    gmax = g;
                }
                if (g < gmin) {
                    gmin = g;
                }
                if (b > bmax) {
                    bmax = b;
                }
                if (b < bmin) {
                    bmin = b;
                }
            }
        }

        // Split this color box at the median point along its
        // longest color dimension
        ColorBox splitBox() {
            if (this.colorCount() < 2)    // this box cannot be split
            {
                return null;
            } else {
                // find longest dimension of this box:
                ColorDimension dim = getLongestColorDimension();

                // find median along dim
                int med = findMedian(dim);

                // now split this box at the median return the resulting new
                // box.
                int nextLevel = level + 1;
                ColorBox newBox = new ColorBox(med + 1, upper, nextLevel);
                this.upper = med;
                this.level = nextLevel;
                this.trim();
                return newBox;
            }
        }

        // Find longest dimension of this color box (RED, GREEN, or BLUE)
        ColorDimension getLongestColorDimension() {
            int rLength = rmax - rmin;
            int gLength = gmax - gmin;
            int bLength = bmax - bmin;
            if (bLength >= rLength && bLength >= gLength) {
                return ColorDimension.BLUE;
            } else if (gLength >= rLength && gLength >= bLength) {
                return ColorDimension.GREEN;
            } else {
                return ColorDimension.RED;
            }
        }

        // Find the position of the median in RGB space along
        // the red, green or blue dimension, respectively.
        int findMedian(@NotNull ColorDimension dim) {
            // sort color in this box along dimension dim:
            Arrays.sort(imageColors, lower, upper + 1, dim.comparator);
            // find the median point:
            int half = count / 2;
            int nPixels, median;
            for (median = lower, nPixels = 0; median < upper; median++) {
                nPixels = nPixels + imageColors[median].count;
                if (nPixels >= half) {
                    break;
                }
            }
            return median;
        }

        ColorNode getAverageColor() {
            int rSum = 0;
            int gSum = 0;
            int bSum = 0;
            int n = 0;
            for (int i = lower; i <= upper; i++) {
                ColorNode ci = imageColors[i];
                int cnt = ci.count;
                rSum = rSum + cnt * ci.red;
                gSum = gSum + cnt * ci.green;
                bSum = bSum + cnt * ci.blue;
                n = n + cnt;
            }
            double nd = n;
            int avgRed = (int) (0.5 + rSum / nd);
            int avgGrn = (int) (0.5 + gSum / nd);
            int avgBlu = (int) (0.5 + bSum / nd);
            return new ColorNode(avgRed, avgGrn, avgBlu, n);
        }

        @Override
        public String toString() {
            String s = this.getClass().getSimpleName();
            s = s + " lower=" + lower + " upper=" + upper;
            s = s + " count=" + count + " level=" + level;
            s = s + " rmin=" + rmin + " rmax=" + rmax;
            s = s + " gmin=" + gmin + " gmax=" + gmax;
            s = s + " bmin=" + bmin + " bmax=" + bmax;
            s = s + " bmin=" + bmin + " bmax=" + bmax;
            return s;
        }
    }

    //	 ---  color dimensions ------------------------

    // The main purpose of this enumeration class is associate
    // the color dimensions with the corresponding comparators.
    enum ColorDimension {
        RED(new redComparator()),
        GREEN(new greenComparator()),
        BLUE(new blueComparator());

        public final Comparator<ColorNode> comparator;

        @Contract(pure = true)
        ColorDimension(Comparator<ColorNode> cmp) {
            this.comparator = cmp;
        }
    }

    // --- color comparators used for sorting colors along different dimensions ---

    static class redComparator implements Comparator<ColorNode> {
        public int compare(@NotNull ColorNode colA, @NotNull ColorNode colB) {
            return colA.red - colB.red;
        }
    }

    static class greenComparator implements Comparator<ColorNode> {
        public int compare(@NotNull ColorNode colA, @NotNull ColorNode colB) {
            return colA.green - colB.green;
        }
    }

    static class blueComparator implements Comparator<ColorNode> {
        public int compare(@NotNull ColorNode colA, @NotNull ColorNode colB) {
            return colA.blue - colB.blue;
        }
    }

    //-------- utility methods -----------

    void listColorNodes(@NotNull ColorNode[] nodes) {
        int i = 0;
        for (ColorNode color : nodes) {
            log.info("Color Node #{}", color.toString());
            i++;
        }
    }

    static class ColorHistogram {

        int[] colorArray = null;
        int[] countArray = null;

        @Contract(pure = true)
        ColorHistogram(int[] color, int[] count) {
            this.countArray = count;
            this.colorArray = color;
        }

        ColorHistogram(@NotNull int[] pixelsOrig) {
            int N = pixelsOrig.length;
            int[] pixelsCpy = new int[N];
            for (int i = 0; i < N; i++) {
                // remove possible alpha components
                pixelsCpy[i] = 0xFFFFFF & pixelsOrig[i];
            }
            Arrays.sort(pixelsCpy);

            // count unique colors:
            int k = - 1; // current color index
            int curColor = - 1;
            for (int value : pixelsCpy) {
                if (value != curColor) {
                    k++;
                    curColor = value;
                }
            }
            int nColors = k + 1;

            // tabulate and count unique colors:
            colorArray = new int[nColors];
            countArray = new int[nColors];
            k = - 1;    // current color index
            curColor = - 1;
            for (int value : pixelsCpy) {
                if (value != curColor) {    // new color
                    k++;
                    curColor = value;
                    colorArray[k] = curColor;
                    countArray[k] = 1;
                } else {
                    countArray[k]++;
                }
            }
        }

        public int[] getColorArray() {
            return colorArray;
        }

        public int[] getCountArray() {
            return countArray;
        }

        public int getNumberOfColors() {
            if (colorArray == null) {
                return 0;
            } else {
                return colorArray.length;
            }
        }

        public int getColor(int index) {
            return this.colorArray[index];
        }

        public int getCount(int index) {
            return this.countArray[index];
        }
    }
}